Readme.txt
Created 14th March 2009 by Nick Young
www.nickbits.co.uk
*********************************

Information
------------
This is a port of Andreas 02. This theme was originally designed by Andreas Viklund (andreasviklund.com) as a free website template. He states (http://andreasviklund.com/about/copyright/) "The free website templates (and all included images and related extras) are released as open source web design. It means that you may use the templates for free for any purpose, personal as well as commercial, without any obligations or conditions."


Please do let me know of any bugs or features you would like to see.

Features
--------
This theme has the following features:

* Site name
* Logo (only in the theme for Drupal 6 at present)
* Slogan
* Mission
* Primary Links
* Secondary Links

Request
--------
The only thing I ask of you if you use this theme is that you leave the links to my and Andreas sites in the footer intact.  Andreas produce the theme and I have ported it to Drupal, both for free.

Install
-------
Install the theme as you would for any other theme.


FAQ
-----
1. How do I place text on the header image?
A. To use the text over the left header image, create a new block as normal.  Then use the region labeled "main_block'".

2.  Can I you add feature xyz?
A.  Yes.  However it depends specifically on the feature and timeframe.  I do not earn any money from this and do it in my spare time.  I attempt to accomodate any changes requested, although this does obviously have to fit in with when I can get the time.

3. How do I change the header image?
A. Navigate to the images directory in the theme folder, and replace front.jpg with your own image.